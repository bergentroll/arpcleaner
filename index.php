<!-- -*-mode: html-*- -->
<?php if (isset($_REQUEST['submit'])) require "handler.php"; ?>

<!DOCTYPE html>
<html>
  <head>
    <title>ARP Cleaner</title>
    <link rel="icon" href="favicon.png" type="image/x-icon">
    <link rel="shortcut icon" href="favicon.png" type="image/x-icon">
    <meta http-equiv="Content-Type" content="text/html"; charset="utf-8"/>
    <meta http-equv="cache-control" content="no-cache"/>
    <link rel="stylesheet" type="text/css" href="style.css" >
  </head>
  <body>
    <div id="wrapper">
      <form class="box" action="<?=$_SERVER['SCRIPT_NAME']?>">
        <h1>Адреса для сброса</h1>
        <label>Адреса MAC и IP-адреса для сброса,<br/>
          разделяя их точкой с запятой или новой строкой.<br/></label>
          <textarea name="addr_list" autofocus="true" cols=50 rows=10
                    ><?=@$_REQUEST['addr_list'];?></textarea><br/>
        <input type="submit" name="submit" value="Сбросить!"/>
      </form>
      <div class="box">
        <h1> Журнал операций</h1>
        <?=@$log?>
      </div>
    </div>
  </body>
</html>
