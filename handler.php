<?php

/* Настройки доступа к свитчу
   Файл включает строковые перемнные $host, $user, $password.*/
require "credentials.php";

$log = '';

// Получаем введённые адреса и перегоняем в массив в виде строк
$addr_list = preg_split('/(;|\n)/', $_REQUEST['addr_list'], -1, PREG_SPLIT_NO_EMPTY);
$addr_list = array_filter(array_map('trim', $addr_list));

// Сортируем полученные адреса по соответствующим массивам
$ip_list = $mac_list = $cisco_mac_list = $trash_list = array();
foreach ($addr_list as $address) {
  if (filter_var($address, FILTER_VALIDATE_IP))
    $ip_list[] = $address;
  else if (preg_match('/^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$/', $address))
    $mac_list[] = $address;
  else if (preg_match('/^([0-9A-Fa-f]{4}.){2}([0-9A-Fa-f]{4})$/', $address))
    $cisco_mac_list[] = $address;
  else
    $trash_list[] = $address;
}

// Переводим стандартные mac-адреса в формат Cisco
$cisco_mac = '';
foreach ($mac_list as $key => $mac) {
  $mac = preg_replace('/(:|-)/', '', $mac);
  $mac = substr($mac, 0, 4) . '.' . substr($mac, 4, 4) . '.' . substr($mac, 8, 4);
  $cisco_mac_list[] = $mac;
  unset($mac_list[$key]);
}

// Подключаемся к свитчу
$socket = fsockopen($host, "23", $errno, $errstr);

if (!$socket) { 
  $log .= '[Ошибка подключения.]<br/>';
} 

else { 
  $log .= '[Подключение установлено.]<br/>';

  // Логинимся
  fputs($socket, $user . "\r");
  fputs($socket, $password . "\r");

  // Очищаем IP по списку
  foreach ($ip_list as $ip) {
    fputs($socket, "clear ip arp $ip\r");
  }

  // Очищаем MAC-адреса по спику
  foreach ($cisco_mac_list as $mac) {
    fputs($socket, "clear mac-address-table dynamic address $mac\r");
  }

  // Разлогиниваемся
  fputs($socket, "quit\r");

  // Пропускаем строку с управляющими символами
  fgets($socket) .'<br/>';

  // Выводим выхлоп терминала
  $timestamp = time();
  while(!feof($socket)) {
    $log .= fgets($socket) .'<br/>';
    if (time() > $timestamp + 15) {
      $log .= '[Таймаут считывания терминала.]<br/>';
      break;
    }
  } 

  // Закрываем подключение
  fclose($socket); 
}

?>
